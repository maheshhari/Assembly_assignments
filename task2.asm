extern printf
extern scanf

section .data
	numformat: db "%d",0  	
	n  : dd 0

section .text
	global main
main:
	push ebp
	mov  ebp, esp

	push n
	push numformat
	call scanf
	mov eax, DWORD[n]
	dec eax
	mov DWORD[n], eax
	mov eax, 0x0
	mov ecx, 0x0
	step:
        inc  eax
	add  ecx, eax
	cmp  eax, DWORD[n]
	jle  step
	push ecx
	push numformat
	call printf

	mov  esp, ebp
	pop  ebp
	ret  
	


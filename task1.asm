extern printf
extern scanf

section .data
	out: db "Greetings, %s ",10,0
	
	in: db "%s",0
	str11: db 0


section .text
	
	global main

main:
	push ebp
	mov  ebp, esp

	push str11
	push in
	call scanf
	
	push str11
	push out
	call printf

	mov  esp, ebp
	pop  ebp
	ret

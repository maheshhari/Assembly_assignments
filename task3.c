#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main(int argc,char *argv[])
{int rot;
 if(getenv("rot")==NULL)
	{rot = 13;}
 else
	{rot = atoi(getenv("rot"));}

 char *s=argv[1];
 int x;
 int l = strlen(s);
 
 for(int i=0;i<l;i++)
	{if (s[i]>=97 && s[i]<=122)
	 	{x = s[i];			
		 x = x+rot;
		 if(x>122)
			x = x-26;
		 s[i]=x;
		}
	
 	else 
	{if(s[i]>=65&&s[i]<=90)
		{x = s[i];
		 x = x+rot;
		 if(x>90)
			x = x-26;		
		 s[i]=x;
		}
	}
}
 printf("%s\n",s);
 return 0;
}
	
	
